﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace TestDP
{
    class TestBindingButton : Button
    {

        public TestBindingButton() : base()
        {
            this.Click += TestBindingButton_Click;
            var prop = DependencyPropertyDescriptor.FromProperty(ChoosenItemProperty, typeof(TestBindingButton));
            prop.AddValueChanged(this, SourceChangedHandler);


        }

        private void TestBindingButton_Click(object sender, RoutedEventArgs e)
        {
            Random r = new Random();
            var val = "update from control " + r.Next(100);
            ChoosenItem = val;
        }


        void SourceChangedHandler(object sender, EventArgs e)
        {
            this.Content = ChoosenItem;
        }


        public static readonly DependencyProperty ChoosenItemProperty = DependencyProperty.Register(
        "ChoosenItem",
        typeof(String),
        typeof(TestBindingButton), new FrameworkPropertyMetadata(null, (FrameworkPropertyMetadataOptions.AffectsRender |
         FrameworkPropertyMetadataOptions.BindsTwoWayByDefault)));
        

       

        public String ChoosenItem
        {
            get
            {
                return (String)GetValue(ChoosenItemProperty);
            }

            set
            {
                SetValue(ChoosenItemProperty, (String)value);
                this.Content = value;

            }
        }



       
    }
}
