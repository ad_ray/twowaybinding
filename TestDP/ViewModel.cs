﻿using Prism.Commands;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace TestDP
{
    class ViewModel : INotifyPropertyChanged
    {

        String test;

        public ViewModel()
        {
            this.SearchCommand = new DelegateCommand<object>(this.OnSearch, this.CanSearch);
        }

        public ICommand SearchCommand
        {
            get; private set;
        }



        public String Test
        {
            get
            {
                return test;
            }
            set
            {
                test = value;
                OnPropertyChanged("Test");
               
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

        private bool CanSearch(object arg)
        {
            return true;
        }

        private void OnSearch(object arg)
        {
            Random r = new Random();
            var val = "update from ViewModel " + r.Next(100);
            Test = val;
        }
    }
}
